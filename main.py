import sys
import time

from requests import HTTPError

from utils import get_statements, save_results


def main(analytics_token):
    try:
        # Retrieve available statements
        statemens = get_statements(analytics_token)

        statements_per_email = {}
        for statement in statemens:
            email = statement["actor"]["mbox"].split("mailto:")[1]
            statements_per_email[email] = statements_per_email.get(email, 0) + 1

        for email in statements_per_email:
            # Built results array
            result = [
                {
                    "column1": int(time.time()),
                    "column2": statements_per_email[email],
                }
            ]

            # Send result to rights engine
            save_results(analytics_token, {"result": result, "context_id": email})
    except HTTPError as error:
        print(error.response.status_code, error.response.text)


if __name__ == "__main__":
    main(sys.argv[1])
